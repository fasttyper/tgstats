from telethon.sync import TelegramClient
from config import api_id, api_hash, session_name


with TelegramClient(session_name, api_id, api_hash) as client:
    dialogs = client.get_dialogs()

    for dialog in dialogs:
        if dialog.is_group:
            print(f"Group : {dialog.name}. ID : {dialog.id}")
