import csv
from telethon.sync import TelegramClient
from config import api_id, api_hash, session_name, target_group

csv_filename = '../group_members.csv'

with TelegramClient(session_name, api_id, api_hash) as client:
    
    members = client.get_participants(target_group)

    with open(csv_filename, 'w', newline='', encoding='utf-8') as csvfile:
        fieldnames = ['User ID', 'Username', 'First Name', 'Last Name']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()

        for member in members:
            writer.writerow({
                'User ID': member.id,
                'Username': member.username,
                'First Name': member.first_name,
                'Last Name': member.last_name
            })