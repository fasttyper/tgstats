# Telegram Group Data Analysis Project

## Overview
This project aims to analyze data from a Telegram group using Python libraries such as Pandas and Matplotlib. The analysis provides insights into message frequency, member activity, language distribution, forwarded messages, and correlation with academic calendars.

## Files
- **analyses/**: Folder containing Python scripts for data analysis.
- **data/**: Directory containing the data files used for analysis.

## Analysis Plan (I'm currently working on them. Hope to update as soon as I finish)
1. **Message Frequency Analysis**:
    - Frequency analysis in months, weeks, days, and years.
2. **Top Message Contributors**:
    - Identify members with the highest message count.
3. **Correlation Analysis**:
    - Explore correlation between top contributors and message frequencies in years.
4. **Language Distribution Analysis**:
    - Analyze the frequency of languages used in messages over time.
5. **Forwarded Message Analysis**:
    - Examine the frequency and patterns of forwarded messages.
6. **Academic Calendar Correlation**:
    - Investigate the correlation between academic calendars and message frequencies.

## Usage
1. Clone the repository:
    ```bash
    git clone https://gitlab.com/fasttyper/tgstats.git
    ```
2. Install the required Python libraries:
    ```bash
    pip install -r requirements.txt
    ```
3. Go to your desired telegram group and export it's data as json in telegram desktop application.
4. Put your json file in the main directory of the project as data.json
5. Run all cells of <b>data_cleaning.ipynb</b> notebook. (This will generate cleaned table messages.csv)
6. Fill <b>config.py</b> python file with the necessary variables.
7. Go through the <b>crawler.ipynb</b> file and follow instructions. After that we will have all the data needed for analyses.
8. Execute the analysis scripts located in the `analyses/` directory.

ENJOY!

My contact on Telegram:
    @brackeys

## Dependencies
- Python 3.11
- Pandas
- Matplotlib
- Telethon

## Contributors
- [fasttyper](https://gitlab.com/fasttyper)